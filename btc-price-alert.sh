#!/bin/bash
echo 'request Bitcoin price';
btc=$(curl https://min-api.cryptocompare.com/data/price?fsym=BTC\&tsyms=USD)
echo 'removing all non digit from the response'
btc=${btc//[^0-9\.]/}
echo 'Bitcoin price is US$ '"$btc"
echo 'removing decimals from the price'
btc=${btc%.*}
echo 'checking if the price within the defined range'
if [ "$btc" -ge 10000 -a "$btc" -lt 14000 ]; then 
        echo 'price is within range, will also post to IFTTT'
        curl -X POST \
        https://maker.ifttt.com/trigger/BTC_PRICE_EVENT/with/key/$IFTTT_KEY \
        -H 'Content-Type: application/json' \
        -d '{"value1": "Bitcoin price is US$ '"$btc"'"}'
  
else echo 'Price is not within range, no alert posted this time'
fi